/* @Autoren: Hendrik und Charlotte

Hier wurden die search_bar_map.dart und search_bar_info.dart zusammengeführt.
Die Suchleiste wird nun in der Map und in den Info-Seiten verwendet.

Hauptsächlich wird das Layout der Suchleiste festgelegt.
Die Funktionen zum Suchen und Filtern sind in den jeweiligen Dateien vorhanden.
______________________________________________________________*/


import 'package:flutter/material.dart';
import 'style.dart';

class CustomSearchBar extends StatelessWidget {
  final TextEditingController?
      controller; // Optional, da es nicht immer benötigt wird
  final Function(String)? onSearch;
  final Function(String)? onChanged;
  final String hintText;

  const CustomSearchBar({
    super.key,
    this.controller,
    this.onSearch,
    this.onChanged,
    this.hintText = 'Suche ...', // Standardtext
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: TextField(
          controller: controller,
          onChanged: (text) {
            // Wenn onChanged definiert ist, rufe es auf
            if (onChanged != null) {
              onChanged!(text);
            }
          },
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: HShColor_LightGrey),
                borderRadius: BorderRadius.circular(30)),
            contentPadding: const EdgeInsets.all(10),
            hintText: hintText,
            suffixIcon: controller != null
                ? IconButton(
                    icon: const Icon(Icons.clear),
                    onPressed: () {
                      controller!.clear();
                      if (onSearch != null) {
                        onSearch!('');
                      }
                    },
                  )
                : null,
            prefixIcon: IconButton(
              icon: const Icon(Icons.search),
              onPressed: () {
                if (controller != null && onSearch != null) {
                  onSearch!(controller!.text);
                }
              },
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
            ),
          ),
          onSubmitted: (value) {
            // Wenn onSearch definiert ist, rufe es auf
            if (onSearch != null) {
              onSearch!(value);
            }
          },
        ),
      ),
    );
  }
}
