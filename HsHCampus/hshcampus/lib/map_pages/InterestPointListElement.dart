/*
Author: @Hendrik
Widget für die Darstellung eines Intrest Points.
Wenn kein Bild übergeben wird, wird das Layout automatisch angepasst und notfalls 
ein Placeholder Image angezeigt.

Die Funktion getImageUrl() wandelt einen gs-Link (Google Cloud Storage) in einen Download-Link um, damit das Bild angezeigt werden kann.




_____________________________________________________________________________*/

//IntrestPointElement.dart
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_storage/firebase_storage.dart';
import '../style.dart';

class InterestPointListElement extends StatelessWidget {
  final String headline;
  final String infoText;
  final String? imagePath;
  final List<String> tags;
  final bool isSingle;

  const InterestPointListElement(
    this.headline,
    this.infoText,
    this.tags, {
    this.imagePath,
    this.isSingle = false,
    super.key,
  });

  Future<String> getImageUrl(String? gsPath) async {
    if (gsPath != null && gsPath.startsWith('gs://')) {
      Reference ref = FirebaseStorage.instance.refFromURL(gsPath);
      return await ref.getDownloadURL();
    }
    return placeholderImage;
  }

  Widget _buildImage(BuildContext context) {
    return Expanded(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: FutureBuilder(
          future: getImageUrl(imagePath),
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.hasData) {
              return CachedNetworkImage(
                imageUrl: snapshot.data!,
                fit: BoxFit.cover,
                placeholder: (context, url) =>
                    const CircularProgressIndicator(),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              );
            }
            return const CircularProgressIndicator(); // Ladeindikator
          },
        ),
      ),
    );
  }

  Widget _buildInfo(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 10),
          Text(headline, style: HshFontMedium),
          Text(
            infoText,
            style: HshFontlight,
            maxLines: isSingle ? 8 : 3,
          ),
          const Spacer(),
          SizedBox(
            height: 20,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: tags.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.all(2),
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 3),
                    decoration: BoxDecoration(
                      border: Border.all(color: HShColor_LightGrey, width: 1),
                    ),
                    child: Center(
                        child: Text(
                      tags[index],
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: HShColor_LightGrey,
                        fontSize: 10,
                      ),
                    )),
                  ),
                );
              },
            ),
          ),
          const SizedBox(height: 15),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 7, bottom: 7),
      height: isSingle ? MediaQuery.of(context).size.height * 0.3 : 150,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            flex: imagePath == null
                ? 2
                : 1, // Flex-Verhältnis anpassen wenn kein Bild vorhanden ist

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (imagePath != null && isSingle) _buildImage(context),
                _buildInfo(context),
              ],
            ),
          ),
          // Zeige das Bild nur an, wenn ein Bildpfad vorhanden ist
          if (imagePath != null && !isSingle) const SizedBox(width: 20),
          if (imagePath != null && !isSingle) _buildImage(context),
          // Verwende das Expanded-Widget, um _buildInfo zu erweitern
        ],
      ),
    );
  }
}
