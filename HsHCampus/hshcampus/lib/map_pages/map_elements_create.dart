/*
Autor: @Hendrik
Funktion für ein BottemSheet, das beim Klicken auf den Button 
Neues Karten-Element hinzufügen aufgerufen wird.
Speichert die Daten in einer Firebase Collection namens map_elements.

Ergänzt von @Charlotte um die Validierung der Eingaben und 
die entsprechende Fehlermeldung.
-> validateFields()

_________________________________________________________*/

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hshcampus/style.dart';
import 'map_elements.dart';

void showAddMapElementBottomSheet(BuildContext context) {
  final titleController = TextEditingController();
  final infoTextController = TextEditingController();
  final xCoordinateController = TextEditingController();
  final yCoordinateController = TextEditingController();
  final buildingController = TextEditingController();
  final tagsController = TextEditingController();

  bool validateFields() {
    // Prüfe, ob alle Felder ausgefüllt sind
    bool isValid = titleController.text.isNotEmpty &&
        infoTextController.text.isNotEmpty &&
        xCoordinateController.text.isNotEmpty &&
        yCoordinateController.text.isNotEmpty &&
        buildingController.text.isNotEmpty &&
        tagsController.text.isNotEmpty;
    return isValid;
  }

  showModalBottomSheet(
    //im Darkmode wird ein dunkleres Grau verwendet
    backgroundColor: Theme.of(context).brightness == Brightness.dark
        ? HShColor_DarkGrey
        : Colors.white,

    context: context,
    isScrollControlled:
        true, // Ermöglicht, dass das Sheet die volle Bildschirmhöhe nutzen kann
    builder: (BuildContext context) {
      return DraggableScrollableSheet(
        expand: false,
        builder: (_, ScrollController scrollController) {
          return Container(
            padding: const EdgeInsets.all(30),
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Neues Karten-Element hinzufügen',
                        style: Theme.of(context).textTheme.headlineLarge,
                      ),
                      IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                  TextField(
                    controller: titleController,
                    decoration: const InputDecoration(
                      labelText: 'Titel',
                      prefixIcon:
                          Icon(Icons.title), // Passendes Icon für den Titel
                    ),
                  ),
                  TextField(
                    controller: infoTextController,
                    decoration: const InputDecoration(
                      labelText: 'Info-Text',
                      prefixIcon: Icon(
                          Icons.info_outline), // Passendes Icon für Info-Text
                    ),
                  ),
                  TextField(
                    controller: xCoordinateController,
                    decoration: const InputDecoration(
                      labelText: 'X-Koordinate',
                      prefixIcon: Icon(
                          Icons.pin_drop), // Passendes Icon für X-Koordinate
                    ),
                    keyboardType: TextInputType
                        .number, // Stelle sicher, dass dies nicht innerhalb von InputDecoration ist.
                  ),
                  TextField(
                    controller: yCoordinateController,
                    decoration: const InputDecoration(
                      labelText: 'Y-Koordinate',
                      prefixIcon: Icon(
                          Icons.pin_drop), // Passendes Icon für X-Koordinate
                    ),
                    keyboardType: TextInputType
                        .number, // Stelle sicher, dass dies nicht innerhalb von InputDecoration ist.
                  ),
                  TextField(
                    controller: buildingController,
                    decoration: const InputDecoration(
                      labelText: 'Gebäude',
                      prefixIcon:
                          Icon(Icons.business), // Passendes Icon für Gebäude
                    ),
                  ),
                  TextField(
                    controller: tagsController,
                    decoration: const InputDecoration(
                      labelText: 'Tags mit Komma trennen',
                      prefixIcon:
                          Icon(Icons.label_outline), // Passendes Icon für Tags
                    ),
                  ),
                  const SizedBox(height: 20),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: HShColor_Orange,
                    ),
                    onPressed: () async {
                      bool isValid = validateFields();
                      if (isValid) {
                        try {
                          double xCoordinate =
                              double.parse(xCoordinateController.text);
                          double yCoordinate =
                              double.parse(yCoordinateController.text);

                          // Tags in ein Array umwandeln und den Titel in Kleinbuchstaben hinzufügen
                          List<String> tags = tagsController.text
                              .toLowerCase()
                              .split(',')
                              .map((tag) => tag.trim())
                              .toList();
                          String title = titleController.text.toLowerCase();

                          // Stelle sicher, dass der Titel auch in den Tags enthalten ist
                          if (!tags.contains(title)) {
                            tags.add(title);
                          }

                          var newMapElement = MapElement(
                            xCoordinate: xCoordinate,
                            yCoordinate: yCoordinate,
                            title: titleController.text,
                            infoText: infoTextController.text,
                            building: buildingController.text,
                            tags: tags,
                          );

                          await FirebaseFirestore.instance
                              .collection('map_elements')
                              .add(newMapElement.toMap());
                          Navigator.pop(context);
                        } catch (e) {
                          print('Fehler beim Hinzufügen: $e');
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text(
                                  'Beim Hinzufügen ist ein Fehler aufgetreten.'),
                              backgroundColor: HShColor_Orange,
                            ),
                          );
                        }
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text(
                                'Bitte fülle alle Felder aus, um ein neues Karten-Element hinzuzufügen.'),
                            backgroundColor: HShColor_Orange,
                          ),
                        );
                      }
                    },
                    child: const Text('Hinzufügen',
                        style: TextStyle(color: Colors.white)),
                  ),
                  const SizedBox(height: 50),
                ],
              ),
            ),
          );
        },
      );
    },
  );
}
