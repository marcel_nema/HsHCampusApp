/* 
Autor: @Hendrik
Klasse für Map-Objekte und Methoden 
zum Konvertieren von Map-Daten in MapElement-Objekte


_______________________________________________________*/

class MapElement {
  final double xCoordinate;
  final double yCoordinate;
  final String title;
  final String building;
  final String? openingHours; // Optional
  final String infoText;
  final String?
      imageUrl; // URL des Bildes was in Intrest Point Liste angezeigt wird
  final String? link; // URL des Links
  final List<String> tags; // Wichtig für die Suchfunktion

  MapElement({
    required this.xCoordinate,
    required this.yCoordinate,
    required this.title,
    required this.building,
    this.openingHours,
    required this.infoText,
    this.imageUrl,
    this.link,
    required this.tags,
  });

  // Konvertiert Map-Daten in ein MapElement-Objekt,
  // wandelt Ganzzahlen in Fließkommazahlen um.
  factory MapElement.fromMap(Map<String, dynamic> map) {
    return MapElement(
      xCoordinate: (map['xCoordinate'] is int)
          ? map['xCoordinate'].toDouble()
          : map['xCoordinate'],
      yCoordinate: (map['yCoordinate'] is int)
          ? map['yCoordinate'].toDouble()
          : map['yCoordinate'],
      title: map['title'],
      building: map['building'],
      openingHours: map['openingHours'],
      infoText: map['infoText'],
      imageUrl: map['imageUrl'],
      link: map['link'],
      tags: List<String>.from(map['tags']),
    );
  }

  // Erstellt eine Map aus MapElement-Eigenschaften
  Map<String, dynamic> toMap() {
    return {
      'xCoordinate': xCoordinate,
      'yCoordinate': yCoordinate,
      'title': title,
      'building': building,
      'openingHours': openingHours,
      'infoText': infoText,
      'imageUrl': imageUrl,
      'link': link,
      'tags': tags,
    };
  }
}
