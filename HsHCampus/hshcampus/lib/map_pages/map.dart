/*
Autoren:
Hendrik Oostinga
  -> Funkttion damit die Map beim start zentriert ist 
  -> Verbinden mit Firebase
    -> Elemente auf der Karte aus Datenbank
  -> Searchbar, Suchfunktion
  -> Auflistung von gefundenen Elementen
  -> SVG-Karte - Design und Einbindung
Felix Langrehr
  -> Map-Grundfunktion über InteractiveViewer
  -> Platzierung von Elementen auf der Karte,
  sodass sie an ihrer Position bleiben
  -> Info-Anzeige beim Anklicken von Punkten 
  (Searchbar ist von Hendrik übernommen)

  Nele Schulze 
  -> HsH-Logo, statisch, wird nicht mit Karte mit skaliert

Diese Datei kümmert sich um die Funktion
der Karte (die Hauptfunktion, manche genutzte
Elemente sind in andere Dateien ausgelagert).

*/
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'map_elements.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../style.dart';
import 'InterestPointListElement.dart';
import 'map_elements_create.dart';
import '../searchbar.dart';

class MapPage extends StatefulWidget {
  final String searchQuery;

  const MapPage({super.key, required this.searchQuery});

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  static _MapPageState? singleton;
  String _localSearchQuery = "";

  //Für den (evtl.) aktuell ausgewählten Punkt
  static bool pointSelected = false;
  static String selectedTitle = "";
  static String selectedInfo = "";
  static List<String> selectedTags = [];

  static double globalMapWidth = 1000;
  static double globalMapHeight = 1000;

  static List<_MapButtonState> mapPoints = [];

  static final TransformationController _transformationController =
      TransformationController();
  final TextEditingController _searchController = TextEditingController();

  final String imagePathLogo = 'Images/HsH_Logo.svg';

  // Damit die Karte beim Start zentriert ist
  @override
  void initState() {
    super.initState();
    _searchController
        .addListener(_filterMapElements); // Listener für die Suchleiste

    _localSearchQuery = widget.searchQuery;
    singleton = this;

    // Kartenmaße
    const double mapWidth = 1000.0;
    const double mapHeight = 1000.0;

    WidgetsBinding.instance.addPostFrameCallback((_) {
      // Größe des Viewports (Bildschirmbereich)
      final Size viewportSize = MediaQuery.of(context).size;

      // Berechnet den Mittelpunkt der Karte
      const double halfMapWidth = mapWidth / 2;
      const double halfMapHeight = mapHeight / 2;

      // Berechnet den Mittelpunkt des Viewports
      final double halfViewportWidth = viewportSize.width / 2;
      final double halfViewportHeight = viewportSize.height / 2;

      // Berechnet die Verschiebung, um die Mitte der Karte in der Mitte des Viewports zu positionieren
      final double offsetX = halfMapWidth - halfViewportWidth;
      final double offsetY = halfMapHeight - halfViewportHeight;
      print('offsetX: $offsetX, offsetY: $offsetY');

      // Setzt die anfängliche Transformation
      _transformationController.value = Matrix4.identity()
        ..translate(-offsetX, -offsetY)
        ..scale(2); // anfänglicher Zoomfaktor
    });
  }

  void updateSearchQuery(String newQuery) {
    _searchController.text = newQuery;
  }

  void _filterMapElements() {
    String searchInput = _searchController.text;
    if (searchInput.isEmpty) {
      _localSearchQuery = "";
    } else {
      _localSearchQuery = searchInput;
    }
    setState(() {});
  }

  // Erstellt eine interaktiven Karte, die dynamisch MapElement-Buttons aus Firestore-Daten anzeigt
  @override
  Widget build(BuildContext context) {
    // Feste Größe der Karte
    final double mapWidth = MediaQuery.of(context).size.width;
    final double mapHeight = MediaQuery.of(context).size.width;
    globalMapHeight = mapWidth;
    globalMapWidth = mapHeight;

    print('');
    return Scaffold(
      body: StreamBuilder<QuerySnapshot>(
        stream: (_localSearchQuery.isEmpty)
            ? FirebaseFirestore.instance.collection('map_elements').snapshots()
            : FirebaseFirestore.instance
                .collection('map_elements')
                .where("tags", arrayContains: _localSearchQuery.toLowerCase())
                .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const Center(child: CircularProgressIndicator());
          }
          var mapElements = snapshot.data!.docs
              .map((doc) =>
                  MapElement.fromMap(doc.data() as Map<String, dynamic>))
              .toList();

          return Scaffold(
            body: Stack(
              children: [
                Center(
                  child: InteractiveViewer(
                    panEnabled: true,
                    minScale: 1.0,
                    maxScale: 5.5,
                    constrained: false,
                    transformationController: _transformationController,
                    onInteractionUpdate: (details) {
                      for (int i = 0; i < mapPoints.length; i++) {
                        mapPoints[i].refresh();
                        globalMapWidth = mapWidth;
                        globalMapHeight = mapHeight;
                      }
                    },
                    child: SizedBox(
                      width: mapWidth,
                      height: mapHeight,
                      child: Stack(
                        children: [
                          SvgPicture.asset(
                            // Karte wechseln im Darkmode
                            Theme.of(context).brightness == Brightness.light
                                ? 'Images/HsH_Campus_Linden.svg'
                                : 'Images/HsH_Campus_Linden_Dark.svg',

                            width: mapHeight,
                            height: mapHeight,
                            fit: BoxFit.cover,
                          ),
                          ...mapElements.map(
                              (e) => _buildMapButton(e, mapWidth, mapHeight)),
                        ],
                      ),
                    ),
                  ),
                ),
                //statisches HsH Logo, liegt über der Karte und wird nicht mitskaliert
                Positioned(
                    left: 10,
                    top: 10,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: SvgPicture.asset(
                        imagePathLogo,
                        // ggf. Alternative Farbe im Darkmode
                        color: Theme.of(context).brightness == Brightness.light
                            ? HShColor_Orange
                            : HShColor_Orange,
                        colorBlendMode: BlendMode.srcIn,
                        width: 150,
                        height: 75,
                      ),
                    )),
              ],
            ),
          );
        },
      ),
      bottomSheet: _buildSearchArea(),
    );
  }

  Widget _buildSearchArea() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).brightness == Brightness.light
                ? Colors.white
                : const Color(0xFF2D2D2D),
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(34),
              topRight: Radius.circular(34),
            ),
          ),
          padding: const EdgeInsets.all(15),
          child: Row(
            children: [
              Expanded(
                child: CustomSearchBar(
                  controller: _searchController,
                  hintText: "Suche auf dem Campus...",
                  onChanged: (value) {
                    setState(() {
                      _localSearchQuery = value.trim();
                    });
                  },
                ),
              ),
              const SizedBox(
                width: 5,
              ),
              // Button zum Hinzufügen von neuen Map-Elementen
              FloatingActionButton(
                elevation: 0,
                shape: const CircleBorder(),
                onPressed: () {
                  showAddMapElementBottomSheet(context);
                },
                backgroundColor: HShColor_Orange,
                mini: true,
                child: const Icon(
                  Icons.add_location_alt_outlined,
                  color: Colors.black,
                ),
              ),
              const SizedBox(
                width: 0,
              ),
            ],
          ),
        ),
        StreamBuilder<QuerySnapshot>(
          // TODO: Hier könnte in der Zukunft Filterung nach Tags eingebaut werden
          stream: FirebaseFirestore.instance
              .collection('map_elements')
              .where("tags", arrayContains: _localSearchQuery.toLowerCase())
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(child: CircularProgressIndicator());
            }
            var mapElements = snapshot.data!.docs
                .map((doc) =>
                    MapElement.fromMap(doc.data() as Map<String, dynamic>))
                .toList();

            bool isSingle = mapElements.length == 1;

            return Container(
              constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height * 0.3),
              // Darkmode verwendet ein dunkleres Grau
              color: Theme.of(context).brightness == Brightness.light
                  ? Colors.white
                  : const Color(0xFF2D2D2D),
              child: ListView.separated(
                shrinkWrap: true,
                itemCount: mapElements.length,
                itemBuilder: (BuildContext context, int index) {
                  MapElement element = mapElements[index];
                  return InterestPointListElement(
                    element.title,
                    element.infoText,
                    element.tags,
                    imagePath: element.imageUrl,
                    isSingle: isSingle,
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return Container(
                    // color: Colors.white,
                    child: const Divider(
                      // color: Colors.white,
                      thickness: 1,
                    ),
                  );
                },
              ),
            );
          },
        ),
        const SizedBox(
          height: 60,
        )
      ],
    );
  }

  /*
  BottomSheet, das dann eingesetzt wird, wenn ein
  Punkt auf der Karte ausgewählt wurde.
  Es zeigt die Infos des ausgewählten Punktes an.
  Die Searchbar ist von Hendriks _buildSearchBar()
  übernommen. 
  Zusätzlich kann dieses BottomSheet geschlossen
  werden, um die Karte wieder besser zu sehen.
  */

  /*
  Widget _buildInfoSheet() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).brightness == Brightness.light
                ? Color.fromARGB(255, 255, 255, 255)
                : const Color(0xFF2D2D2D),
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(34),
              topRight: Radius.circular(34),
            ),
          ),
          padding: const EdgeInsets.all(15),
          child: Row(
            children: [
              Expanded(
                child: CustomSearchBar(
                  onChanged: (value) {
                    setState(() {
                      pointSelected = false;
                      _localSearchQuery = value.trim();
                    });
                  },
                ),
              ),
              const SizedBox(
                width: 5,
              ),
              FloatingActionButton(
                elevation: 0,
                shape: const CircleBorder(),
                onPressed: () {
                  showAddMapElementBottomSheet(context);
                },
                backgroundColor: HShColor_Orange,
                mini: true,
                child: const Icon(
                  Icons.add_location_alt_outlined,
                  color: Colors.black,
                ),
              ),
              const SizedBox(
                width: 0,
              ),
            ],
          ),
        ),
        Container(
          constraints: BoxConstraints(
              maxHeight: MediaQuery.of(context).size.height * 0.3),
          child: ListView(
            shrinkWrap: true,
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Theme.of(context).brightness == Brightness.light
                        ? Colors.white
                        : HShColor_DarkGrey),
                child: Padding(
                  padding: const EdgeInsets.all(0),
                  child: Column(children: [
                    IconButton(
                      onPressed: () {
                        setState(() {
                          pointSelected = false;
                        });
                      },
                      icon: const Icon(
                        Icons.close,
                      ),
                    ),
                    Container(
                      height: 10,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: Text(
                        selectedTitle,
                        style: const TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      height: 10,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: Text(
                        selectedInfo,
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      height: 10,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.9,
                      height: 20, // Definiert eine feste Höhe für die ListView
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: selectedTags.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                            padding: const EdgeInsets.all(2),
                            child: Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 3),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: HShColor_LightGrey, width: 1),
                              ),
                              child: Center(
                                  child: Text(
                                selectedTags[index],
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: HShColor_LightGrey,
                                  fontSize: 10,
                                ),
                              )),
                            ),
                          );
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 70,
                    )
                  ]),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
  */

  // Positioniert eine Schaltfläche auf der Karte,
  // zeigt Dialog mit Titel und Information bei Klick.
  Widget _buildMapButton(
      MapElement element, double mapWidth, double mapHeight) {
    // Größe des Icons
    double iconSize = 20.0;

    // Berechnet die neue Position, so dass das Icon zentriert ist

    return MapButton(
      iconSize: iconSize,
      element: element,
      searchController: _searchController, // Übergebe den _searchController
      updateSearchCallback: updateSearchQuery, // Übergebe die Callback-Funktion
    );
  }

  void refresh() {
    setState(() {});
  }
}

/*
Die MapButton's werden auf der Karte an ihren
jeweiligen Positionen platziert und sorgen beim
Antippen dafür, dass als BottomSheet das InfoSheet
eingesetzt wird, um die Infos anzuzeigen.
*/
class MapButton extends StatefulWidget {
  TextEditingController searchController;
  double iconSize;
  MapElement element;

  // Callback-Funktion, um die Suchleiste zu aktualisieren,
  // wenn ein Punkt auf der Karte ausgewählt wird
  Function(String) updateSearchCallback;
  MapButton({
    super.key,
    required this.iconSize,
    required this.element,
    required this.searchController,
    required this.updateSearchCallback,
  });

  @override
  State<MapButton> createState() => _MapButtonState();
}

class _MapButtonState extends State<MapButton> {
  @override
  void initState() {
    _MapPageState.mapPoints.add(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double left =
        (widget.element.xCoordinate / 1000) * _MapPageState.globalMapWidth -
            ((widget.iconSize / 2) + 14);
    double top =
        (widget.element.yCoordinate / 1000) * _MapPageState.globalMapHeight -
            ((widget.iconSize / 2) + 13);
    return Positioned(
      left: left,
      top: top,
      child: IconButton(
        padding: const EdgeInsets.all(2),
        iconSize: calculateSize(),
        onPressed: () {
          _MapPageState.pointSelected = true;
          _MapPageState.selectedTitle = widget.element.title;
          _MapPageState.selectedInfo = widget.element.infoText;
          _MapPageState.selectedTags = widget.element.tags;
          _MapPageState.singleton!.refresh();
          widget.updateSearchCallback(
              widget.element.title); // Aktualisiere die Suchleiste
        },
        splashRadius: 5 + calculateSize() * 1.3,
        icon: const Icon(
          Icons.location_on,
          color: HShColor_Orange,
        ),
      ),
    );
  }

  /*
  Um die Punkte von außerhalb zu aktualisieren, 
  nötig um ihre Größe dem Zoom des InteractiveViewers
  entsprechend anzupassen.
  */
  void refresh() {
    setState(() {});
  }

  @override
  void dispose() {
    _MapPageState.mapPoints.remove(this);
    super.dispose();
  }

  // Funktion zur Berechnung der Größe der Punkte auf der Karte
  // anhand des aktuellen Zooms des InteractiveViewers.
  double calculateSize() {
    double currentScale =
        _MapPageState._transformationController.value.getMaxScaleOnAxis();
    double scaleDownMultiplier = 3.5;
    return widget.iconSize - (currentScale - 1) * scaleDownMultiplier;
  }
}
