/*
@Autorin: Charlotte

Diese Seite enthält einen GridView mit den Kategorien, die auf die jeweiligen Kategorie-Seiten weiterleitet.
Über den Hinzufügen-Button wird die AddInfo-Seite aufgerufen, auf der man Infos für die Akkordeons hinzufügen kann.
Die Farben der Schrift und des Icons werden aus der style.dart übernommen.
Es wird auf die SearchBar aus der search_bar_info.dart zurückgegriffen und auf die AccordionList aus der accordion_list.dart.
Außerdem wird auf Firebase zugegriffen, um die Artikel aus Firestore zu laden und nach Suchbegriffen zu filtern.

Grundstruktur von _loadArtikelFromFirestore() und _filterAccordionElements() wurden von @Hendrik übernommen. 
Angepasst hat @Charlotte, dass die Suchtags auch Wörter aus dem Titel enthalten und dass auch
nach Wortteilen gesucht werden kann.

_______________________________________________________________________________________________________________________*/

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hshcampus/info_pages/article.dart';
import 'package:hshcampus/style.dart';
import 'general_category_page.dart';
import '../searchbar.dart' as search;
import 'accordion_list.dart';
import 'add_info.dart';

class CategoryPage extends StatefulWidget {
  final Function(String) onCategorySelected;

  const CategoryPage({super.key, required this.onCategorySelected});

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  final TextEditingController _searchController = TextEditingController();
  List<AccordionElement> _accordionElements = [];
  bool _isSearching = false; // Wird auf true gesetzt, wenn nach etwas gesucht wird, damit dann das GridView durch die AccordionList ersetzt wird

  @override
  void initState() {
    super.initState();
    _searchController.addListener(_onSearchChanged); // Listener für die Suchleiste
    _loadArtikelFromFirestore();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Kategorien',
          style: Theme.of(context).textTheme.headlineLarge,
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        scrolledUnderElevation: 0,
      ),
      body: Column(
        children: [
          search.CustomSearchBar(
            hintText: 'Suche in Kategorien ...',
            controller: _searchController,
            onSearch: (query) => _filterAccordionElements(query), 
            // Callback-Funktion, die aufgerufen wird, wenn nach etwas gesucht wird
          ),
          Expanded(
            child: _isSearching
                ? _accordionElements.isEmpty
                    ? Center(child: Text('Keine Suchergebnisse gefunden.', style: Theme.of(context).textTheme.headlineMedium))
                    : AccordionList(elements: _accordionElements) // Wenn nach etwas gesucht wird, wird die AccordionList angezeigt
                : _buildGridView(),
          ),
          const SizedBox(height: 65),
        ],
      ),
    );
  }

  // OnSearchChanged wird aufgerufen, wenn sich der Text in der Suchleiste ändert
  void _onSearchChanged() {
    String searchInput = _searchController.text;
    if (searchInput.isEmpty) {
      _isSearching = false;
      // Wenn is Searching false ist, wird das GridView angezeigt
      _loadArtikelFromFirestore();
    } else {
      _isSearching = true;
      _filterAccordionElements(searchInput);
    }
  }

  // Hier wird der Filter aufgerufen, wenn der Text in der Suchleiste geändert wird
  void _filterAccordionElements(String searchInput) {
    searchInput = searchInput.toLowerCase();
    setState(() {
      _isSearching = searchInput.isNotEmpty;
    });

    if (searchInput.isEmpty) {
      _loadArtikelFromFirestore();
    } else {
      FirebaseFirestore.instance
          .collection('artikel')
          .snapshots()
          .listen((snapshot) {
        List<AccordionElement> searchResults = snapshot.docs
            .map((doc) {
              Artikel artikel = Artikel.fromFirestore(doc);
              return AccordionElement(
                doc.id,
                artikel.titel,
                artikel.infoText,
                artikel.downloadLink ?? '',
                artikel.kategorie,
                artikel.tags.join(', '),
              );
            })
            // Hier wurde die Filterung angepasst, sodass auch nach Wortteilen gesucht werden kann
            .where((element) =>
                element.tags.toLowerCase().contains(searchInput) ||
                element.title.toLowerCase().contains(searchInput))
            .toList();

        if (mounted) {
          setState(() {
            _accordionElements = searchResults;
          });
        }
      });
    }
  }

  // Lädt die Artikel aus Firestore und speichert sie in der Instanzvariable _accordionElements
  void _loadArtikelFromFirestore() {
    FirebaseFirestore.instance
        .collection('artikel')
        .snapshots()
        .listen((snapshot) {
      List<AccordionElement> loadedArticles = snapshot.docs.map((doc) {
        Artikel artikel = Artikel.fromFirestore(doc);
        return AccordionElement(
          artikel.id,
          artikel.titel,
          artikel.infoText,
          artikel.downloadLink ?? '',
          artikel.kategorie,
          artikel.tags.join(', '),
        );
      }).toList();

      if (mounted) {
        setState(() {
          _accordionElements = loadedArticles;
        });
      }
    });
  }

  // Ein eigenes Widget für die Kategorie-Buttons, welches den Titel, das Icon und einen isEnabled-Parameter annimmt
  // Das Widget ist eine Funktion, die ein ElevatedButton zurückgibt
  Widget _buildCategoryButton(String title, IconData icon,
      {bool isEnabled = true}) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ElevatedButton(
        onPressed: isEnabled
            ? () {
              if (title == 'Hinzufügen') {
                  // Wird separat behandelt, da es nicht zu einer Kategorie-Seite navigiert
                  Navigator.push(
                    // Navigiert zur AddInfo-Seite
                    context,
                    MaterialPageRoute(
                      builder: (context) => const AddInfo(),
                    ),
                  );
                } else {
                widget.onCategorySelected(title); 
                // Aufruf der Callback-Funktion, die in der Konstruktor-Parameterliste übergeben wurde
                // Die Callback-Funktion wird in der main.dart aufgerufen und navigiert zur GeneralCategoryPage
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        GeneralCategoryPage(category: title),
                  ),
                );
              }
            }
            : null, // Wenn isEnabled false ist, wird der Button deaktiviert
        style: ElevatedButton.styleFrom(
          backgroundColor: isEnabled 
              // Bei Darkmode wird ein dunkleres Grau verwendet
              ? Theme.of(context).brightness == Brightness.light
                  ? HShColor_BackgroundGrey
                  : const Color(0xFF2D2D2D)
              : const Color(0xFFE0E0E0), // Wenn isEnabled false ist, wird die Hintergrundfarbe auf ein helles Grau gesetzt
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          padding: const EdgeInsets.symmetric(vertical: 16),
        ),
        child: FittedBox(
          fit: BoxFit.scaleDown,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                icon,
                size: 24.0,
                color: isEnabled ? HShColor_Orange : Colors.grey,
              ),
              Text(
                title,
                style: TextStyle(
                  fontSize: 14.0,
                  // Bei Darkmode wird ein weißer Text verwendet
                  color: isEnabled
                      ? Theme.of(context).brightness == Brightness.light
                          ? HShColor_Grey
                          : Colors.white
                      : Colors.grey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Erstellt ein GridView mit den Kategorien
  Widget _buildGridView() {
    return GridView.count(
      // Erstellt ein Grid mit 2 Spalten, die sich an die Breite des Bildschirms anpassen
      crossAxisCount: 2,
      childAspectRatio: 3 / 1.75, // Verhältnis Breite / Höhe
      shrinkWrap: true, // Das Grid passt sich der Größe der Kinder an
      children: [
        // Erstelle die Buttons mit dem _buildCategoryButton-Widget
        // Per default sind alle Buttons aktiviert
        _buildCategoryButton('Allgemein', Icons.info_outline),
        _buildCategoryButton('Studiengänge & Kurse', Icons.edit),
        _buildCategoryButton('Verpflegung', Icons.local_cafe_outlined),
        _buildCategoryButton('Verkehr & Parken', Icons.emoji_transportation_outlined),
        _buildCategoryButton('Technik', Icons.computer_outlined),
        _buildCategoryButton('Bibliothek', Icons.local_library_outlined),
        _buildCategoryButton('Veranstaltungen', Icons.calendar_month_outlined),
        _buildCategoryButton('Studienberatung', Icons.chat_bubble_outline_outlined),
        _buildCategoryButton('International', Icons.language_outlined),
        _buildCategoryButton('Notfälle', Icons.error_outline),
        _buildCategoryButton('Hinzufügen', Icons.add_comment_outlined),
      ],
    );
  }

  @override
  void dispose() {
    // Der Controller muss manuell entsorgt werden, da er sonst im Speicher bleibt
    _searchController.dispose();
    super.dispose(); // Aufruf der Super-Methode, um die State-Instanz zu entsorgen
  }
}
