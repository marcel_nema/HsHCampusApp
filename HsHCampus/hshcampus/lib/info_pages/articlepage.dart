/*
Autor: @Charlotte 
Charlotte hat das Grundgerüst erstellt und die Firebase-Abfragen aus
anderen Dateien kopiert und eingefügt.

@Hendrik hat das Layout und den Content vervollständigt (den Charlotte kreiert hat). 
________________________________________________________________________*/
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hshcampus/info_pages/article.dart';
import 'package:hshcampus/style.dart';

class ArticlePage extends StatefulWidget {
  final String articleId;

  const ArticlePage({super.key, required this.articleId});

  @override
  _ArticlePageState createState() => _ArticlePageState();
}

class _ArticlePageState extends State<ArticlePage> {
  Artikel? _artikel;

  @override
  void initState() {
    super.initState();
    _loadArtikelFromFirestore();
  }

  void _loadArtikelFromFirestore() {
    FirebaseFirestore.instance
        .collection('artikel')
        .doc(widget.articleId)
        .get()
        .then((doc) {
      if (doc.exists) {
        setState(() {
          _artikel = Artikel.fromFirestore(doc);
        });
      } else {
        print('Artikel nicht gefunden'); // Debug-Ausgabe
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_artikel == null) {
      return Scaffold(
        appBar: AppBar(
          scrolledUnderElevation: 0,
        ),
        body: const Column(
          children: [
            Center(child: CircularProgressIndicator()),
            Text('Artikel wird geladen...',
                style: TextStyle(
                    color: HShColor_Grey, fontWeight: FontWeight.bold)),
          ],
        ),
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 20, 30, 50),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // zurück-Button
                  Align(
                    alignment: Alignment.topLeft,
                    child: IconButton(
                      icon: const Icon(
                        Icons.arrow_back,
                        color: HShColor_Orange,
                      ),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          _artikel!.titel,
                          style: Theme.of(context).textTheme.headlineLarge,
                        ),
                        const SizedBox(height: 20),
                        Text(
                          _artikel!.text,
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                        const SizedBox(height: 20),
                        const SizedBox(height: 20),
                        Text(
                          _artikel!.tags.join(', '),
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
