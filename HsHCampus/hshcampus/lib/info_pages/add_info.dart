/*
@Autor: Hendrik
Hendrik hat die Grundstruktur der Seite erstellt und die Firebase-Abfragen erstellt.
-> _loadArtikelFromFirestore()
-> _filterAccordionElements()

Überarbeitet von @Charlotte, die die Kategorien hinzugefügt hat und den Hinzufügen-Button gelockt hat 
für den Fall, dass nicht alle Felder ausgefüllt wurden. Außerdem wurde die Funktion _submitArticle() geändert, sodass die Suchtags
auch Wörter aus dem Titel enthalten. Es wurde ein eigenes Widget für die TextFormFields erstellt, um es auslagern zu können.


Erstellt ein StatefulWidget Formularfeldern und Artikel-Upload-Funktion zu Firestore.

__________________________________________________________________________________________________________________________________*/

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hshcampus/style.dart';

class AddInfo extends StatefulWidget {
  const AddInfo({super.key});

  @override
  _AddInfoState createState() => _AddInfoState();
}

class _AddInfoState extends State<AddInfo> {
  final TextEditingController _titelController = TextEditingController();
  final TextEditingController _textController = TextEditingController();
  final TextEditingController _infoTextController = TextEditingController();
  final TextEditingController _tagsController = TextEditingController();
  final List<String> _categories = [
    'Allgemein',
    'Studiengänge & Kurse',
    'Verpflegung',
    'Verkehr & Parken',
    'Technik',
    'Bibliothek',
    'Veranstaltungen',
    'Studienberatung',
    'International',
    'Notfälle',
  ];
  String? _selectedCategory;
  bool buttonActive = false; // Wird auf true gesetzt, wenn alle Felder ausgefüllt sind

  @override
  void initState() {
    super.initState();
    // addListener() wird aufgerufen, wenn sich der Text in einem Feld ändert
    _titelController.addListener(_checkFields);
    _infoTextController.addListener(_checkFields);
    _textController.addListener(_checkFields);
    _tagsController.addListener(_checkFields);
  }

  @override
  void dispose() {
    _titelController.dispose();
    _infoTextController.dispose();
    _textController.dispose();
    _tagsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hinzufügen',
            style: Theme.of(context).textTheme.headlineLarge),
        centerTitle: true,
        iconTheme: const IconThemeData(color: HShColor_Orange),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
          hoverColor: Colors
              .transparent, // Entfernt den Schatteneffekt beim Überfahren mit der Maus
          splashColor:
              Colors.transparent, // Entfernt den Schatteneffekt beim Anklicken
          highlightColor:
              Colors.transparent, // Entfernt den Schatteneffekt beim Anklicken
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            _buildTextFormField('Titel', Icons.title, _titelController, _checkFields),
            const SizedBox(height: 10), // Abstand zwischen den Feldern
            _buildTextFormField('Info-Text (kurz)', Icons.info, _infoTextController, _checkFields),
            const SizedBox(height: 10),
            _buildTextFormField('Text', Icons.text_fields, _textController, _checkFields),
            const SizedBox(height: 10),
            _buildDropdownButtonFormField(),
            const SizedBox(height: 10),
            _buildTextFormField('Tags (Wichtig für Suche)', Icons.tag, _tagsController, _checkFields),
            const SizedBox(height: 20),
            _buildAddButton(),
          ],
        ),
      ),
    );
  }

  Widget _buildDropdownButtonFormField() {
    return DropdownButtonFormField(
      value: _selectedCategory,
      decoration: InputDecoration(
        labelText: 'Kategorie',
        icon: const Icon(Icons.grid_view),
        border: const OutlineInputBorder(),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: HShColor_Orange),
        ),
        // Farbe des Labels, wenn es nicht oben steht (angepasst an Light- und Darkmode):
        labelStyle: Theme.of(context).brightness == Brightness.light 
          ? const TextStyle(color: HShColor_DarkGrey)
          : const TextStyle(color: HShColor_LightGrey),
        floatingLabelStyle: const TextStyle(color: HShColor_Orange),
      ),
      // Farbe des Dropdown-Menüs (angepasst an Light- und Darkmode):
      dropdownColor: Theme.of(context).brightness == Brightness.light 
        ? HShColor_BackgroundGrey
        : const Color(0xFF2D2D2D),
      items: _categories.map((String category) {
        return DropdownMenuItem(
          value: category,
          child: Text(
            category, 
            style: TextStyle(color: Theme.of(context).brightness == Brightness.light 
              ? HShColor_Grey 
              : HShColor_LightGrey
            )
          ),
        );
      }).toList(),
      onChanged: (String? newValue) {
        setState(() {
          _selectedCategory = newValue!;
        });
      },
    );
  }

  void _checkFields() {
    // Überprüft, ob alle Felder ausgefüllt sind
    bool allFieldsFilled = _titelController.text.isNotEmpty &&
        _infoTextController.text.isNotEmpty &&
        _textController.text.isNotEmpty &&
        _tagsController.text.isNotEmpty &&
        _selectedCategory != null;

    setState(() {
      buttonActive = allFieldsFilled;
    });
  }

  // Erstellt ein TextFormField mit einem Icon, einem Label und einem Border
  Widget _buildTextFormField(String labelText, IconData icon, TextEditingController controller, Function onChanged) {
    return TextFormField(
      cursorColor: HShColor_Orange,
      controller: controller,
      onChanged: (_) => onChanged(),
      decoration: InputDecoration(
        labelText: labelText,
        icon: Icon(icon),
        border: const OutlineInputBorder(),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: HShColor_Orange),
        ),
      labelStyle: Theme.of(context).brightness == Brightness.light // Farbe des Labels, wenn es nicht oben steht (angepasst an Light- und Darkmode)
          ? const TextStyle(color: HShColor_DarkGrey)
          : const TextStyle(color: HShColor_LightGrey),
      floatingLabelStyle: const TextStyle(color: HShColor_Orange),
      ),
    );
  }

  Widget _buildAddButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        backgroundColor: HShColor_Orange,
      ),
      onPressed: buttonActive ? () {
        _submitArticle();
        _titelController.clear();
        _infoTextController.clear();
        _textController.clear();
        _tagsController.clear();
        setState(() {
          buttonActive = false;
          _selectedCategory = null;
        });
        // Wenn der Artikel erfolgreich gespeichert wurde, werden die Felder geleert und der Button deaktiviert
      } : () {
        // Wenn der Button nicht aktiv ist, wird eine Snackbar angezeigt, die darauf hinweist, dass nicht alle Felder ausgefüllt sind
        // Bzw welche Felder nicht ausgefüllt sind
        String missingFields = '';
        if (_titelController.text.isEmpty) {
          missingFields += 'Titel, ';
        }
        if (_infoTextController.text.isEmpty) {
          missingFields += 'Info-Text, ';
        }
        if (_textController.text.isEmpty) {
          missingFields += 'Text, ';
        }
        if (_tagsController.text.isEmpty) {
          missingFields += 'Tags, ';
        }
        if (_selectedCategory == null) {
          missingFields += 'Kategorie, ';
        }
        missingFields = missingFields.substring(0, missingFields.length - 2); // Entfernt das letzte Komma und Leerzeichen
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Folgende Felder müssen noch ausgefüllt werden: $missingFields'), backgroundColor: HShColor_Orange),
        );
      },
      child: const Text('Hinzufügen',
          style: TextStyle(color: Colors.white)),
    );
  }

  void _submitArticle() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;

    // Erstelle Suchtags
    List<String> tags = _tagsController.text
        .split(',')
        .map((t) => t.trim().toLowerCase())
        .toList();
    List<String> searchTags = tags.expand((t) => createSearchTags(t)).toList();

    List<String> titleSearchTags = _titelController.text
        .toLowerCase()
        .split(' ')
        .where((word) => word.isNotEmpty)
        .toList();

    // Kombiniere Titel-Suchtags mit den vorhandenen Tags
    List<String> combinedSearchTags = [
      ...searchTags,
      ...titleSearchTags
    ]; // ... bedeutet, dass die Elemente der Liste einzeln hinzugefügt werden und nicht die Liste selbst

    // Erstelle ein neues Artikel-Objekt in Form einer Map
    // Datenstruktur, um verschiedene Informationen in Form von Schlüssel-Wert-Paaren zu speichern
    Map<String, dynamic> artikelData = {
      'titel': _titelController.text.trim(), // trim() entfernt Leerzeichen am Anfang und Ende des Strings
      'text': _textController.text.trim(),
      'infoText': _infoTextController.text.trim(),
      'kategorie': _selectedCategory,
      'tags': tags,
      'searchTags': combinedSearchTags,
      // Füge hier zusätzliche Felder hinzu, falls benötigt
    };

    try {
      await firestore.collection('artikel').add(artikelData);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Artikel erfolgreich gespeichert'), backgroundColor: HShColor_Orange),
      );
    } catch (e) {
      print('Fehler beim Speichern des Artikels: $e');
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Fehler beim Speichern des Artikels: $e')),
      );
    }
  }

  List<String> createSearchTags(String tag) {
    // Zerlegt das Tag in einzelne Wörter und wandelt jedes in Kleinbuchstaben um
    List<String> words =
        tag.toLowerCase().split(' ').where((t) => t.isNotEmpty).toList();
    return words;
  }
}
