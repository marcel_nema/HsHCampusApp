/*
@Autor: Felix
Hat eine Grundstruktur erstellt, die ExpansionPanelList und ExpansionPanel enthält. 

@Hendrik hat die Artikel mit Firebase verbunden. (_loadArtikelFromFirestore()) 

Überarbeitet von @Charlotte, die die Suchfunktion in die general_category_page.dart verschoben hat und eine id für die Artikel hinzugefügt hat.
Außerdem wurde das Widget _buildPanel auf mehrere Widgets aufgeteilt, z.B. _buildPanelBody, _buildTrailingIcon, _buildMoreInfoButton und _buildDeleteButton,
um die Lesbarkeit zu verbessern. Die Löschen-Funktion wurde hinzugefügt sowie die Mehr-Infos-Funktion, die auf die Artikel-Seite verlinkt.
Es wurde ExpansionPanel durch ExpansionTile ersetzt, damit das default trailing Icon (arrow) entfernt und durch ein eigenes Icon ersetzt werden kann.

Die Farben der Schrift und des Icons werden aus der style.dart übernommen.

Erstellt eine interaktive Akkordeon-Liste, die Artikel aus Firestore lädt und nach Suchbegriffen filtert.
_______________________________________________________________________________________________________*/

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'articlepage.dart';
import 'article.dart';
import 'dart:async';
import '../style.dart';

class AccordionList extends StatefulWidget {
  final List<AccordionElement>? elements;
  final String? category;

  const AccordionList({super.key, this.elements, this.category});
  // Erstellt eine Akkordeon-Liste mit den übergebenen Elementen und/oder der übergebenen Kategorie
  // super.key ist ein optionales Argument, das an das übergeordnete Widget weitergegeben wird und für die Identifizierung des Widgets verwendet wird
  // elements ist eine optionale Liste von AccordionElementen, die angezeigt werden sollen
  // category ist eine optionale Kategorie, nach der gefiltert werden soll

  @override
  _AccordionListState createState() => _AccordionListState();
  // Erstellt einen neuen Zustand für das Widget
  // verantwortlich für das Laden der Daten, das Reagieren auf Benutzerinteraktionen und das Aktualisieren der Benutzeroberfläche
}

// Hier wird ein neues 'StatefulWidget' namens 'AccordionList' definiert, welches seinen Zustand ändern kann

class _AccordionListState extends State<AccordionList> {
  List<AccordionElement> _accordionElements = [];
  StreamSubscription? _streamSubscription;
  // StreamSubscription ist ein Objekt, das auf einen Stream hört und bei jedem Ereignis eine Funktion ausführt
  // Hier wird der StreamSubscription verwendet, um auf Änderungen in Firestore zu hören

  @override
  void initState() {
    // initState() wird aufgerufen, wenn das Widget zum ersten Mal erstellt wird
    super.initState();
    if (widget.elements == null) {
      // Wenn keine Elemente übergeben wurden, werden die Elemente aus Firestore geladen
      _loadArtikelFromFirestore();
    }
  }

  void _loadArtikelFromFirestore() {
    Query query = FirebaseFirestore.instance.collection('artikel');
    // Erstellt eine Abfrage, die alle Artikel aus der Sammlung 'artikel' enthält
    if (widget.category != null) {
      query = query.where('kategorie',
          isEqualTo: widget
              .category); // Wenn eine Kategorie übergeben wurde, wird die Abfrage danach gefiltert
    }
    _streamSubscription = FirebaseFirestore.instance
        .collection('artikel')
        .where('kategorie', isEqualTo: widget.category) // Filter nach Kategorie
        .snapshots()
        .listen((snapshot) {
      if (!mounted) {
        return; // Überprüfen ob das Widget gemountet ist / noch im Baum ist, damit setState() aufgerufen werden kann
      }

      List<AccordionElement> loadedArticles = snapshot.docs.map((doc) {
        // 'snapshots()' liefert einen Stream von Daten, der mit 'listen()' abgehört werden kann
        Artikel artikel = Artikel.fromFirestore(doc);
        return AccordionElement(
          doc.id,
          artikel.titel,
          artikel.infoText,
          artikel.downloadLink ?? '',
          artikel.kategorie,
          artikel.tags.join(', '),
        );
      }).toList();

      if (mounted) {
        // Überprüfen ob das Widget gemountet ist / noch im Baum ist, damit setState() aufgerufen werden kann
        setState(() {
          // setState() aktualisiert den Zustand des Widgets mit den neuen Daten
          _accordionElements =
              loadedArticles; // Die geladenen Artikel werden in die Liste der AccordionElemente gespeichert
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children:
              _buildPanelList(), // Hier wird die Liste der ExpansionPanels erstellt
        ),
      ),
    );
  }

  // Erstellt eine Liste von ExpansionPanels
  List<Widget> _buildPanelList() {
    List<AccordionElement> elements = widget.elements ?? _accordionElements;
    // Wenn keine Elemente übergeben wurden, werden die Elemente aus Firestore verwendet
    // widget.elements ist eine optionale Liste von AccordionElementen, die angezeigt werden sollen
    // _accordionElements ist eine Liste von AccordionElementen, die aus Firestore geladen wurden

    // Erstellt eine Map, also eine Liste von Key-Value-Paaren, die in diesem Fall aus dem Index und dem Element besteht
    return elements.asMap().entries.map<Widget>((entry) { // iteriert über die Elemente in der Liste
      int idx = entry.key;
      AccordionElement element = entry.value;
      return _buildAccordion(element, idx);
    }).toList(); // wandelt die Map in eine Liste um
  }

  // Erstellt ein einzelnes ExpansionPanel
  Widget _buildAccordion(AccordionElement element, int idx) {
    bool isExpanded = element.isExpanded;
    Color borderColor = isExpanded ? HShColor_Orange : Colors.transparent;
    // Es soll nur umrandet sein, wenn es erweitert ist

    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        // Bestimmt die Farben basierend auf dem aktuellen Theme
        color: Theme.of(context).brightness == Brightness.light
            ? HShColor_BackgroundGrey
            : const Color(0xFF2D2D2D),
        border: Border.all(color: borderColor, width: 2),
        borderRadius: BorderRadius.circular(5),
      ),
      child: ExpansionTile(
        initiallyExpanded: element
            .isExpanded, // Bestimmt, ob das Element erweitert ist oder nicht
        title: Text(
          element.title,
          style: Theme.of(context).textTheme.headlineSmall,
        ),
        trailing: _buildTrailingIcon(element
            .isExpanded), // Erstellt ein Icon, das anzeigt, ob das Element erweitert ist oder nicht
        onExpansionChanged: (bool expanded) {
          setState(() {
            element.isExpanded = expanded;
          });
        },
        children: [
          _buildPanelBody(element),
        ],
      ),
    );
  }

  // Erstellt ein Icon, das anzeigt, ob das Element erweitert ist oder nicht (Plus- oder Minus-Icon)
  Widget _buildTrailingIcon(bool isExpanded) {
    // Bestimme die Farben basierend auf dem aktuellen Theme
    Color borderColor;
    Color iconColor;

    // Bestimme die Farben basierend auf dem aktuellen Theme
    if (Theme.of(context).brightness == Brightness.light) {
      borderColor = isExpanded ? const Color(0xFFCB4923) : HShColor_Grey;
      iconColor = isExpanded ? const Color(0xFFCB4923) : HShColor_Grey;
    } else {
      borderColor = isExpanded ? const Color(0xFFCB4923) : HShColor_LightGrey;
      iconColor = isExpanded ? const Color(0xFFCB4923) : HShColor_LightGrey;
    }

    return Container(
      width: 24,
      height: 24,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border.all(color: borderColor),
        borderRadius: BorderRadius.circular(2),
      ),
      child: Icon(
        isExpanded ? Icons.remove : Icons.add,
        color: iconColor,
        size: 20,
      ),
    );
  }

  // Erstellt den Inhalt des ExpansionPanels, der angezeigt wird, wenn das Element erweitert ist
  Widget _buildPanelBody(AccordionElement element) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (element.link.isNotEmpty) ...[
            // Wenn der Link nicht leer ist, wird er angezeigt
            const SizedBox(height: 10),
            Text('Link: ${element.link}'),
          ],
          //const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
            child: Text(element.body), // Der Text wird angezeigt
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment
                .spaceBetween, // Buttons werden links und rechts ausgerichtet
            children: [
              _buildDeleteButton(element.id),
              _buildMoreInfoButton(element.id),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildMoreInfoButton(String articleId) {
    return Align(
      alignment: Alignment.bottomRight,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(HShColor_Orange),
          padding: MaterialStateProperty.all<EdgeInsets>(
              // Abstand zwischen Text und Rand, damit der Button nicht zu klein ist
              const EdgeInsets.symmetric(horizontal: 8, vertical: 5)),
        ),
        onPressed: () {
          // Öffnet die Artikel-Seite mit der übergebenen ID
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ArticlePage(articleId: articleId),
            ),
          );
        },
        child: const Text('Mehr Infos', style: TextStyle(color: Colors.white)),
      ),
    );
  }

  Widget _buildDeleteButton(String articleId) {
    return Align(
      alignment: Alignment.bottomLeft,
      child: TextButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
        ),
        onPressed: () {
          // Öffnet einen Dialog, um zu bestätigen, dass der Artikel gelöscht werden soll
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: const Text('Artikel löschen'),
                content:
                    const Text('Möchtest du diesen Artikel wirklich löschen?'),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(
                          context); // Schließt den Dialog, ohne den Artikel zu löschen
                    },
                    child: const Text('Abbrechen'),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                      // Schließt den Dialog und löscht den Artikel
                      // Confirmation Snackbar
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text('Artikel gelöscht'),
                          backgroundColor: HShColor_Orange,
                          duration: Duration(seconds: 2),
                        ),
                      );
                      FirebaseFirestore.instance
                          .collection('artikel')
                          .doc(articleId)
                          .delete();
                    },
                    child: const Text('Löschen'),
                  ),
                ],
              );
            },
          );
        },
        child: Icon(
          Icons.delete,
          color: Theme.of(context).brightness == Brightness.light
              ? HShColor_Grey
              : HShColor_LightGrey,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _streamSubscription?.cancel();
    super.dispose();
  }
}

class AccordionElement {
  String id; // Hinzugefügte ID
  String title;
  String body;
  String link;
  String kategorie;
  String tags;
  bool isExpanded;

  AccordionElement(
      this.id, this.title, this.body, this.link, this.kategorie, this.tags,
      [this.isExpanded = false]);
}
