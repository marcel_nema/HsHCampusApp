/*
@Autorin: Charlotte

Diese Klasse bekommt den Namen der Kategorie übergeben, auf welche geklickt wurde,
und baut daraus eine Seite mit einer Liste von Accordion-Elementen, die nach der Kategorie gefiltert wurden (siehe AccordionList).
Es werden eine AppBar mit dem Namen der Kategorie, eine Searchbar aus searchbar.dart und ein AccordionList-Widget erstellt.
Die Farben der Schrift und des Icons werden aus der style.dart übernommen.
_onSearchChanged überprüft, ob etwas in die Suchleiste eingegeben wurde und ruft dann _filterAccordionElements auf.

________________________________________________________________________________________________________*/

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hshcampus/style.dart';
import 'accordion_list.dart';
import '../searchbar.dart' as search;
import 'article.dart';

class GeneralCategoryPage extends StatefulWidget {
  final String category;

  const GeneralCategoryPage({super.key, required this.category});

  @override
  _GeneralCategoryPageState createState() => _GeneralCategoryPageState();
}

class _GeneralCategoryPageState extends State<GeneralCategoryPage> {
  final TextEditingController _searchController = TextEditingController();
  List<AccordionElement> _accordionElements = [];
  bool _isSearching =
      false; // Wird auf true gesetzt, wenn nach etwas gesucht wird, damit die AccordionList durch die gefilterte Liste ersetzt wird

  @override
  void initState() {
    super.initState();
    _searchController.addListener(_onSearchChanged);
    _loadArtikelFromFirestore();
    _filterAccordionElements('');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.category,
            style: Theme.of(context).textTheme.headlineLarge),
        centerTitle: true,
        iconTheme: const IconThemeData(color: HShColor_Orange),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
          hoverColor: Colors.transparent, // Entfernt den Schatteneffekt beim Überfahren mit der Maus
          splashColor: Colors.transparent, // Entfernt den Schatteneffekt beim Anklicken
          highlightColor: Colors.transparent, // Entfernt den Schatteneffekt beim Anklicken
        ),
        scrolledUnderElevation: 0,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: search.CustomSearchBar(
              hintText: 'Suche in ${widget.category} ...',
              controller: _searchController,
              onSearch: _filterAccordionElements,
            ),
          ),
          Expanded(
            // Wenn es Artikel in der Kategorie gibt, wird die AccordionList angezeigt, 
            // ansonsten ein Text, der darauf hinweist, dass noch keine Artikel vorhanden sind
            child: _accordionElements.isNotEmpty
                ? AccordionList(elements: _accordionElements)
                : const Center(
                    child: Text(
                      'Für diese Kategorie sind noch keine Informationen vorhanden.\nArtikel können auch selber über den "Hinzufügen"-Tab erstellt werden.',
                      style: TextStyle(
                        fontSize: 16,
                        color: HShColor_Orange,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
            ),
          const SizedBox(height: 65),
        ],
      ),
    );
  }

  // Hier wird der Filter aufgerufen, wenn der Text in der Suchleiste geändert wird
  void _onSearchChanged() {
    String searchInput = _searchController.text;
    if (searchInput.isEmpty) {
      _isSearching = false;
      _filterAccordionElements(null); // Null als Suchbegriff übergeben, um alle Artikel der Kategorie anzuzeigen
    } else {
      _isSearching = true;
      _filterAccordionElements(searchInput);
    }
  }

  void _loadArtikelFromFirestore() {
    Query query = FirebaseFirestore.instance.collection('artikel');
    if (widget.category.isNotEmpty) {
      query = query.where('kategorie', isEqualTo: widget.category);
    }

    // Hier fügen wir einen Listener hinzu, um die Daten tatsächlich zu laden
    query.snapshots().listen((snapshot) {
      if (snapshot.docs.isNotEmpty) {
        setState(() {
          _accordionElements = snapshot.docs.map((doc) {
            Artikel artikel = Artikel.fromFirestore(doc);
            return AccordionElement(
              doc.id,
              artikel.titel,
              artikel.infoText,
              artikel.downloadLink ?? '',
              artikel.kategorie,
              artikel.tags.join(', '),
            );
          }).toList();
        });
      } else {
        setState(() {
          _accordionElements = []; // Setzt die Liste auf leer, wenn keine Artikel vorhanden sind
        });
      }
    });
  }

  void _filterAccordionElements(String? searchInput) {
    searchInput ??= ''; // Wenn searchInput null ist, wird ein leerer String zugewiesen
    setState(() {
      _isSearching = searchInput!.isNotEmpty; // Wenn searchInput nicht leer ist, wird _isSearching auf true gesetzt
    });

    if (searchInput.isEmpty) {
      _loadArtikelFromFirestore();
    } else {
      FirebaseFirestore.instance
          .collection('artikel')
          .where('kategorie', isEqualTo: widget.category)
          .snapshots()
          .listen((snapshot) {
        List<AccordionElement> searchResults = snapshot.docs
            .map((doc) {
              // Die Suchergebnisse werden in eine Liste von Accordion-Elementen umgewandelt
              // snapshot.docs ist eine Liste von Dokumenten, die von der Datenbank zurückgegeben wird
              Artikel artikel = Artikel.fromFirestore(doc);
              return AccordionElement(
                doc.id, // Die ID des Dokuments wird als ID des Accordion-Elements verwendet
                artikel.titel,
                artikel.infoText,
                artikel.downloadLink ?? '',
                artikel.kategorie,
                artikel.tags.join(', '), // Tags werden mit Komma getrennt
              );
            })
            .where((element) =>
                element.tags.toLowerCase().contains(searchInput!.toLowerCase()) ||
                element.title.toLowerCase().contains(searchInput.toLowerCase()))
            .toList();
        if (mounted) {
          // Überprüfen ob das Widget gemountet ist / noch im Baum ist, damit setState() aufgerufen werden kann
          setState(() {
            _accordionElements = searchResults;
          });
        }
      });
    }
  }

  @override
  void dispose() {
    _searchController.removeListener(_onSearchChanged); // Listener entfernen
    _searchController.dispose();
    super.dispose();
  }
}
