/*
@Autor: Hendrik
Hat eine Klasse für Artikel hinzugefügt, die die Daten aus 
Firestore in Artikel Objekten speichert.

_______________________________________________________________________________________________________*/

import 'package:cloud_firestore/cloud_firestore.dart';

class Artikel {
  final String id;
  final String titel;
  final String infoText;
  final String text;
  final String kategorie;
  final List<String> tags;
  final List<String>? bilder;
  final String? headerBild;
  final String? downloadLink;
  final String? datenbankVerweis;

  Artikel.fromFirestore(DocumentSnapshot doc)
      : id = doc.id,
        titel = (doc.data() as Map<String, dynamic>)['titel'] ?? '',
        infoText = (doc.data() as Map<String, dynamic>)['infoText'] ?? '',
        text = (doc.data() as Map<String, dynamic>)['text'] ?? '',
        kategorie = (doc.data() as Map<String, dynamic>)['kategorie'] ?? '',
        tags = (doc.data() as Map<String, dynamic>)['tags'] != null
            ? List<String>.from((doc.data() as Map<String, dynamic>)['tags'])
            : [],
        bilder = (doc.data() as Map<String, dynamic>)['bilder'] != null
            ? List<String>.from((doc.data() as Map<String, dynamic>)['bilder'])
            : [],
        headerBild = (doc.data() as Map<String, dynamic>)['headerBild'],
        downloadLink = (doc.data() as Map<String, dynamic>)['downloadLink'],
        datenbankVerweis =
            (doc.data() as Map<String, dynamic>)['datenbankVerweis'];

  Map<String, dynamic> toFirestore() {
    return {
      'titel': titel,
      'infoText': infoText,
      'longText': text,
      'kategorie': kategorie,
      'tags': tags,
      'bilder': bilder,
      'headerBild': headerBild,
      'downloadLink': downloadLink,
      'datenbankVerweis': datenbankVerweis
    };
  }
}
