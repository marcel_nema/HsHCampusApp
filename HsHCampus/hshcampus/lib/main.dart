/*
Autor: 
@Felix hat die grundlegende Navigation mit BottomNavigationBar
erstellt und designtechnisch dem Entwurf aus Figma angeglichen.

@Hendrik 
hat den Code für die Firebase-Initialisierung geschrieben.
Material App definiert mit Schriftarten, Farben und Darkmode Settings 
Design von der Navbar angepasst

________________________________________________________________________________*/

import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:hshcampus/map_pages/map.dart';
import 'package:hshcampus/info_pages/categorypage.dart';
import 'package:hshcampus/style.dart';
import 'firebase_options.dart';
import 'more_pages/about.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  try {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
    runApp(
      const HshCampusApp(),
    );
  } catch (e) {
    // TODO Debugging
    print("Firebase-Initialisierung fehlgeschlagen: $e");
  }
}

class HshCampusApp extends StatelessWidget {
  const HshCampusApp({super.key});

  @override
  Widget build(BuildContext context) {
    const Color fontColorLightmode = HShColor_Grey;
    const Color fontColorDarkmode = Colors.white;
    return MaterialApp(
      title: 'HsH Campus App',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        fontFamily: 'FFUnitPro',
        canvasColor: Colors.white,
        textTheme: const TextTheme(
          /*
          displayLarge: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w500,
            color: HShColor_Grey,
          ),
          displayMedium: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: HShColor_Grey,
          ),
          displaySmall: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            color: HShColor_Grey,
          ),*/
          headlineLarge: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w500,
            color: fontColorLightmode,
          ),
          headlineMedium: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: fontColorLightmode,
          ),
          headlineSmall: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: fontColorLightmode,
          ),
          titleLarge: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: fontColorLightmode,
          ),
          bodyLarge: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w300,
            color: fontColorLightmode,
          ),
          bodyMedium: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w300,
            color: fontColorLightmode,
          ),
          bodySmall: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w300,
            color: fontColorLightmode,
          ),
        ),
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.orange,
        fontFamily: 'FFUnitPro',
        canvasColor: Colors.white,
        cardColor: HShColor_BackgroundGrey,
        textTheme: const TextTheme(
          /*
          displayLarge: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
          displayMedium: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
          displaySmall: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),*/
          headlineLarge: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w500,
            color: fontColorDarkmode,
          ),
          headlineMedium: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: fontColorDarkmode,
          ),
          headlineSmall: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: fontColorDarkmode,
          ),
          titleLarge: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: fontColorDarkmode,
          ),
          bodyLarge: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w300,
            color: fontColorDarkmode,
          ),
          bodyMedium: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w300,
            color: fontColorDarkmode,
          ),
          bodySmall: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w300,
            color: fontColorDarkmode,
          ),
        ),
      ),
      home: const HshCampusHome(),
      routes: {
        '/map': (context) => const MapPage(searchQuery: ''),
        '/categories': (context) =>
            CategoryPage(onCategorySelected: (category) {}),
        '/about': (context) => About(),
      },
    );
  }
}

class HshCampusHome extends StatefulWidget {
  const HshCampusHome({super.key});

  @override
  _HshCampusHomeState createState() => _HshCampusHomeState();
}

class _HshCampusHomeState extends State<HshCampusHome> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  int _selectedIndex = 0;
  final String _searchQuery = '';

  void _onItemTapped(int index) {
    if (index == _selectedIndex) {
      return;
    }
    setState(() {
      _selectedIndex = index;
    });

    switch (index) {
      case 0:
        _navigatorKey.currentState?.pushNamed('/map');
        break;
      case 1:
        _navigatorKey.currentState?.pushNamed('/categories');
        break;
      case 2:
        _navigatorKey.currentState?.pushNamed('/about');
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: () async {
          if (_navigatorKey.currentState!.canPop()) {
            _navigatorKey.currentState!.pop();
            return false;
          }
          return true;
        },
        child: Navigator(
          key: _navigatorKey,
          initialRoute: '/',
          onGenerateRoute: (RouteSettings settings) {
            WidgetBuilder builder;
            switch (settings.name) {
              case '/':
              case '/map':
                builder = (BuildContext context) =>
                    MapPage(searchQuery: _searchQuery);
                break;
              case '/categories':
                builder = (BuildContext context) =>
                    CategoryPage(onCategorySelected: (category) {});
                break;
              case '/about':
                builder = (BuildContext context) => About();
                break;
              default:
                throw Exception('Invalid route: ${settings.name}');
            }
            return MaterialPageRoute(
              builder: builder,
              settings: settings,
            );
          },
        ),
      ),
      bottomSheet: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          BottomNavigationBar(
            backgroundColor:
                // Bei Darkmode wird ein dunkleres Grau verwendet
                Theme.of(context).brightness == Brightness.light
                    ? Colors.white
                    : const Color(0xFF2D2D2D),
            type: BottomNavigationBarType.fixed,
            //backgroundColor: Colors.white,
            selectedItemColor: HShColor_Orange,
            unselectedItemColor:
                // Bei Darkmode wird weiß verwendet als Schriftfarbe
                Theme.of(context).brightness == Brightness.light
                    ? HShColor_Grey
                    : Colors.white,
            iconSize: 30,
            currentIndex: _selectedIndex,
            selectedLabelStyle: HshFontMedium,
            unselectedLabelStyle: HshFontMedium,
            elevation: 0,
            onTap: _onItemTapped,
            items: const [
              BottomNavigationBarItem(
                icon: Icon(Icons.map),
                label: 'Karte',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.info_outline),
                label: 'Infos',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.more_horiz),
                label: 'Mehr',
              ),
            ],
          ),
        ],
      ),
    );
  }
}
