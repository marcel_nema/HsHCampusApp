/*
Autor: @Hendrik
Hendrik hat die Seite erstellt, sie besteht nur aus Content und Layout Widgets , welche den App-Namen,
den Teamnamen mit den Namen der Teammitglieder sowie eine Auflistung
aller verwendeten Widgets und Flutter-Packages/Plug-ins enthält.
________________________________________________________________________*/
import 'package:flutter/material.dart';
import 'package:hshcampus/style.dart';

class About extends StatelessWidget {
  const About({super.key});

  @override
  Widget build(BuildContext context) {
    String text = """
Sonstige Arbeiten:\n

Kamera, Regie, Schnitt und Effects des HsH Campus Videos \n
Marcel \n

Schauspieler: \n
Hendrik \n

Informationen zu den Kategorien:\n
Marcel und Charlotte\n

Infopoints Content: \n
Nele und Charlotte\n

Appidee Powerpoint:\n
von Hendrik und Charlotte\n

Auflistung aller Widgets:\n
Marcel\n


Folder:info_pages:\n
accordion_list.dart (Artikel in ausklappbaren Akkordeon-Elementen darstellen):\n
<Padding>: Verwendet, um Abstand innerhalb des Widgets hinzuzufügen, insbesondere um die Column-Widgets in den Akkordeons zu umschließen\n
Column: Ermöglicht die vertikale Anordnung von Widgets\n
<ExpansionTile>: Ersetzt das ExpansionPanel, um eine interaktive Liste zu erstellen, die sich aus- und einklappen lässt\n
<AlertDialog>: Zeigt einen Dialog (Popup-Fenster), hier zum Bestätigen des Löschens eines Artikels\n
<ScaffoldMessenger>: Verwendet, um Feedback (Snackbar) zu Aktionen wie dem Löschen eines Artikels zu geben\n\n
add_info.dart (Artikel in den Kategorien anlegen und Eingabe-Seite Funktionen):\n
<SingleChildScrollView>: Ermöglicht es den Kontent auch bei geöffneter Tastatur oder größerem Inhalt zu sehen und einfach nach unten zu wischen\n
<TextFormField> Ermöglicht die Freitexteingabe mit vielen gestalterischen Möglichkeiten durch <InputDecoration>\n
 <DropdownButtonFromFiel>, <OutlineInputBorder> und <InputDecoration>: Erstellt eine Auswahlmöglichkeit bei der Eintragung einer neuen Information in die Kategorieseite, mit welcher man dann eine der angegebenen Kategorien auswählen kann, da man keine neuen erstellen soll.\n
<Elevated Button>: schönerer Button mit mehr Möglichkeiten im vergleich zu einem TextButton\n
<ScaffoldManager>: Response, bei fehlerhafter oder erfolgreicher Speicherung\n\n
acticle.dart (in der Kategorie aufgerufene Artikel):\n
<Map>, um die gegebenen Informationen zu verwalten\n\n
articlepage.dart (Darstellung der Seite von den, in der Kategorie aufgerufenen Artikeln):\n
<SingleChildScrollView> auch wieder, um es scrollable zu machen, wenn verschiebung durch Keyboard kommt\n
<Stack> Sortierung, Gliederung\n
<CircularProgerssBar>: Ladeanimation für die Bilder bei der Suchleiste unten bei der Karte, falls das Bild nicht direkt laden sollte\n\n
categorypage.dart (Kategorieseite und Funktion):\n
<CustomSearchBar>: Kategorieseite Suchleiste in schön\n
<List>: im Akkordeon-Stil\n
<MaterialPageRoute>: first Screen <-> second Screen Zurechtfindung bei Öffnung einer Information / Artikels / Akkordeonelements\n
<FittedBox>: sorgt dafür, dass sich die Widgets gut aneinander anpassen von der skalierung her (ScaleDown wurde benutzt)\n
<GridView>: Wurde benutzt um die erste Darstellung (die Kategorien) darzustellen mit den verschiedenen Texten und Icons\n\n
general_category_page.dart:\n
ähnliche Widgets: AccordionElement, AccordionList\n
<Scaffold>: Stellt das Grundgerüst der Seite dar, einschließlich der AppBar und des Body\n
<TextEditingController>: Ein Controller für die Texteingabe in der Suchleiste\n
<StreamSubscription>: Wird verwendet, um auf Änderungen in der Firestore-Datenbank zu reagieren und die Artikel entsprechend zu aktualisieren\n
Folder: map_pages\n\n
InterestPointListElement.dart (befasst sich mit der Darstellung der Informationen von aufgerufenen Informationspunkten “InterestPoints”, welche man auf der Karte sieht und aufrufen kann):\n
<ClipRRect>: abgerundete Ecken bei der Information zu einem Info-Punkt auf der Karte -> Anzeige unten, welche dann Text und ein Bild anzeigt\n
<CircularProgessIndicator> again Ladefunktion, damit man wenigstens etwas sieht\n
<ListView.builder>: effiziente Darstellung großer oder dynamischer Listen. Erstellt hier eine horizontale Liste von Tags. \n
<FirebaseStorage>: Zugriff auf Firebase Storage, um Bild-URLs abzurufen\n
<DraggableScrollableSheet> und <SingleChildScrollView> damit man noch richtig navigieren kann, wenn die Tastatur oder zu viele angezeigte Elemente da sind, um noch nach unten wischen zu können\n\n
main.dart:\n
<WillPopScope> verhindert, dass man bei einer neuen Seitenerstellung den “Zurück”-Button benutzen kann\n
<BottomNavigationBar> und <WidgetBuilder> befassen sich hier mit der erstellung der Grundlegenden Seite bzw. Navigation\n\n\n

Wer was gemacht hat:\n
map_elements.dart:\n
Klassen:\n
MapElement: Hendrik\n
map.dart:\n
Klassen:\n
MapPage: Hendrik & Felix\n
_MapPageState: Hendrik & Felix & Nele\n
MapButton: Felix \n
_MapButtonState: Hendrik & Felix\n
Funktionen:\n
initState: Felix & Hendrik\n
dispose: Felix\n
calculateSize: Felix\n
map_elements_create.dart:\n
Funktionen:\n
showAddMapElementBottomSheet: Hendrik & Charlotte\n\n


main.dart:\n
Klassen:\n
HshCampusApp: Hendrik & Charlotte\n
HshCampusHome: Charlotte & Felix\n
_HshCampusHomeState: Charlotte & Felix\n
Funktionen:\n
main: Felix & Hendrik\n
_onItemTapped: Charlotte\n
Main: Felix & Hendrik\n
InterestPointListElement.dart:\n
Klassen:\n
InterestPointListElement: Hendrik\n
info_database_helper.dart:\n
Klassen:\n
DatabaseHelper: Hendrik\n
accordion_list.dart:\n
Klassen:\n
AccordionList: Felix & Charlotte\n
_AccordionListState: Felix & Charlotte\n
AccordionElement: Hendrik & Charlotte\n
Funktionen:\n
initState: Hendrik\n
_loadArtikelFromFirestore: Hendrik\n
dispose: Hendrik\n\n


categorypage.dart:\n
Klassen:\n
CategoryPage: Charlotte\n
_CategoryPageState: Charlotte\n
Funktionen:\n
initState: Charlotte\n
_onSearchChanged: Charlotte\n
_filterAccordionElements: Hendrik & Charlotte\n
_loadArtikelFromFirestore: Hendrik\n
dispose: Charlotte\n
article.dart:\n
Klassen:\n
Artikel: Hendrik\n
add_info.dart:\n
Klassen:\n
AddInfo: Charlotte & Hendrik\n
_AddInfoState: Charlotte & Hendrik\n
Funktionen:\n
initState: Charlotte\n
dispose: Charlotte\n
_checkFields: Charlotte.\n
_submitArticle: Charlotte & Hendrik\n
articlepage.dart:\n
Klassen:\n
ArticlePage: Charlotte\n
_ArticlePageState: Charlotte & Hendrik\n\n


Funktionen:\n
initState: Charlotte\n
_loadArtikelFromFirestore: Hendrik\n
general_category_page.dart:\n
Klassen:\n
GeneralCategoryPage: Charlotte\n
_GeneralCategoryPageState: Charlotte\n
GeneralCategoryPage. Charlotte\n
Funktionen:\n
initState: Charlotte\n
_onSearchChanged: Charlotte\n
_loadArtikelFromFirestore: Charlotte\n
_filterAccordionElements: Charlotte\n
dispose: Charlotte\n
search_bar_info.dart:\n
Klassen:\n
SearchBar: Charlotte\n
searchbar_map.dart:\n
Klassen:\n
CustomSearchField: Hendrik\n
firebase_options.dart:\n
Klassen:\n
DefaultFirebaseOptions: Wird automatisch erstellt von Firebase \n\n\n

Packages die wir verwendet haben: \n\n

provider: \n
Das provider Paket verwaltet den Navigationszustand, ermöglicht konsistente Tab-Auswahl und Seitenanzeige in der gesamten App\n\n

flutter_svg: \n
damit die Campus Map vektorbasiert dargestellt werden kann, also egal bei welcher Größe immer scharf ist.\n\n

cached_network_image: \n
lädt und cacht Bilder bei den Interest Points, verbessert die Ladezeit\n\n

cloud_firestore: \n
Für den Zugriff auf Cloud Firestore-Datenbank von Firebase, damit Artikel und Map-Elemente cloudbasiert in Echtzeit geladen werden können\n\n

firebase_core: \n
Für die Initialisierung und Verwendung von Firebase-Diensten\n\n

firebase_storage: \n
Zum Abrufen von Bilddateien der Map-Elemente.\n\n

flutter_launcher_icons: \n
Zum Anpassen des App-Icons für Android und iOS.\n\n


""";
    return Scaffold(
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          const Image(image: AssetImage('Images/A_Gebaeude.png')),
          const SizedBox(height: 20),
          // Headline
          Padding(
            padding: const EdgeInsets.all(50.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'HsH Campus App',
                  style: Theme.of(context)
                      .textTheme
                      .headlineLarge
                      ?.copyWith(color: HShColor_Orange, fontSize: 40),
                ),
                const SizedBox(height: 10),
                Text(
                  'designed und programmiert von Team 1',
                  style: Theme.of(context).textTheme.headlineSmall,
                ),
                Text(
                  'Charlotte Bärhold\n Felix	Langrehr\n Marcel Nema\n Nele Schulze\n Hendrik Oostinga\n',
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                const SizedBox(height: 20),
                Text(
                  text,
                  style: Theme.of(context).textTheme.bodySmall,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
