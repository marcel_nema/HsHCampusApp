import 'package:flutter/material.dart';

// Farben
const Color HShColor_LightGrey = Color.fromRGBO(182, 182, 182, 1);
const Color HShColor_Grey = Color.fromRGBO(87, 82, 80, 1);
const Color HShColor_DarkGrey = Color(0xFF2D2D2D);
const Color HShColor_BackgroundGrey = Color.fromRGBO(237, 237, 237, 1);
const Color HShColor_Orange = Color.fromRGBO(203, 73, 35, 1);

// Platzhalterbilder
const String placeholderImage = "images/Book_closed.png";

// Schriftarten

const TextStyle HshFontMedium = TextStyle(
  fontFamily: 'FFUnitPro',
  fontWeight: FontWeight.w500,
);

const TextStyle HshFontlight = TextStyle(
  fontFamily: 'FFUnitPro',
  fontWeight: FontWeight.w300,
);
